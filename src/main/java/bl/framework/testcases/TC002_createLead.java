package bl.framework.testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import bl.framework.api.ProjectMethods;

public class TC002_createLead extends ProjectMethods {

	// @Test(invocationCount=2, invocationTimeOut=50000)
	// @Test(invocationCount=2, timeOut=30000)
	// @Test(invocationCount=2, threadPoolSize=2
	// @Test(groups="Smoke")
	// @Parameters({ "url", "password", "username" })
	@Test(dataProvider = "getData1")
	public void createLead(String cname, String fname, String lname) {
		// login();
//		startApp("chrome", url);
//		WebElement eleUsername = locateElement("id", "username");
//		clearAndType(eleUsername, uname);
//		WebElement elePassword = locateElement("id", "password");
//		clearAndType(elePassword, psw);
//		WebElement eleLogin = locateElement("class", "decorativeSubmit");
//		click(eleLogin);
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Create Lead").click();
		driver.findElementById("createLeadForm_companyName").sendKeys(cname);
		driver.findElementById("createLeadForm_firstName").sendKeys(fname);
		driver.findElementById("createLeadForm_lastName").sendKeys(lname);
		driver.findElementByName("submitButton").click();
//		WebElement eleCRMSFA = locateElement("link", "CRMSFA");
//		click(eleCRMSFA);
//		WebElement eleCreateLead = locateElement("link", "Create Lead");
//		click(eleCreateLead);
//		WebElement eleCompany = locateElement("createLeadForm_companyName");
//		click(eleCompany);

//		WebElement eleFirstName = locateElement("id", "createLeadForm_firstName");
//		clearAndType(eleFirstName, "Yogesh");
//		locateElement("id", "createLeadForm_lastName");

	}

	@DataProvider(name = "getData")
	public String[][] getData() {

		String[][] data = new String[2][3];
		data[0][0] = "Company Name";
		data[0][1] = "First Name";
		data[0][2] = "Last Name";

		data[1][0] = "Company Name";
		data[1][1] = "Yogesh";
		data[1][2] = "Kumar";
		return data;
	}

	@DataProvider(name = "getData1")
	public String[][] getData1() {

		String[][] data = new String[2][3];
		data[0][0] = "second company";
		data[0][1] = "one Name";
		data[0][2] = "two Name";

		data[1][0] = "third company";
		data[1][1] = "three Name";
		data[1][2] = "four Name";
		return data;
	}
}