package bl.framework.testcases;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.testng.annotations.Test;

import bl.framework.api.ProjectMethods;

@Test
public class TC003_mergeLead extends ProjectMethods {
	public void editLead() {
		login();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Create Lead").click();
		driver.findElementById("createLeadForm_companyName").sendKeys("Company Name");
		driver.findElementById("createLeadForm_firstName").sendKeys("Yogesh");
		driver.findElementById("createLeadForm_lastName").sendKeys("Kumar");
		driver.findElementByName("submitButton").click();
		driver.findElementByXPath("//a[text()='Merge Leads']").click();
		// driver.findElementByXPath("//input[@id='ComboBox_partyIdFrom']").sendKeys("Yogesh");
		driver.findElementByXPath("//*[@id=\"ext-gen606\"]").click();
		Set<String> allWindows = driver.getWindowHandles();
		List<String> ls = new ArrayList();
		ls.addAll(allWindows);
		driver.switchTo().window(ls.get(1));
		System.out.println(driver.getTitle());
	}

}
