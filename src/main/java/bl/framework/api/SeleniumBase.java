package bl.framework.api;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Select;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import bl.framework.base.Browser;
import bl.framework.base.Element;

public class SeleniumBase implements Browser, Element {

	public RemoteWebDriver driver;
	public int i = 1;

	@Override
	public void startApp(String url) {

	}

	@Override
	public void startApp(String browser, String url) {
		if (browser.equalsIgnoreCase("chrome")) {
			System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
			driver = new ChromeDriver();
		} else if (browser.equalsIgnoreCase("firefox")) {
			System.setProperty("webdriver.gecko.driver", "./drivers/geckodriver.exe");
			driver = new FirefoxDriver();
		}
		driver.manage().window().maximize();
		driver.get(url);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		System.out.println("The browser " + browser + " launched successfully");
		takeSnap();
	}

	@Override
	public WebElement locateElement(String locatorType, String value) {
		switch (locatorType) {
		case "id":
			return driver.findElementById(value);
		case "name":
			return driver.findElementByName(value);
		case "class":
			return driver.findElementByClassName(value);
		case "xpath":
			return driver.findElementByXPath(value);
		case "link":
			return driver.findElementByLinkText(value);
		case "PartialLiank":
			return driver.findElementByPartialLinkText(value);
		case "tag":
			return driver.findElementByTagName(value);
		default:
			break;
		}
		return null;
	}

	@Override
	public WebElement locateElement(String value) {

		return driver.findElementById(value);

	}

	@Override
	public List<WebElement> locateElements(String type, String value) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void switchToAlert() {
		driver.switchTo().alert();

	}

	@Override
	public void acceptAlert() {
		driver.switchTo().alert().accept();
		takeSnap();

	}

	@Override
	public void dismissAlert() {
		driver.switchTo().alert().dismiss();
		takeSnap();

	}

	@Override
	public String getAlertText() {
		return driver.switchTo().alert().getText();
	}

	@Override
	public void typeAlert(String data) {
		driver.switchTo().alert().sendKeys(data);
		takeSnap();

	}

	@Override
	public void switchToWindow(int index) {

	}

	@Override
	public void switchToWindow(String title) {

	}

	@Override
	public void switchToFrame(int index) {

	}

	@Override
	public void switchToFrame(WebElement ele) {
		driver.switchTo().frame(ele);
		takeSnap();

	}

	@Override
	public void switchToFrame(String idOrName) {
		driver.switchTo().frame(idOrName);
		takeSnap();

	}

	@Override
	public void defaultContent() {
		driver.switchTo().defaultContent();
		takeSnap();

	}

	@Override
	public boolean verifyUrl(String url) {
		String currentUrl = driver.getCurrentUrl();
		if (currentUrl.equals(url)) {
			System.out.println("The current URL" + url + "is verified");
		} else
			System.out.println("The current URL is not verified");
		takeSnap();
		return false;
	}

	@Override
	public boolean verifyTitle(String title) {

		String currentTitle = driver.getTitle();
		try {
			if (currentTitle.equals(title)) {
				System.out.println("The current title" + title + "is verified");
			} else
				System.out.println("The current title is not verified");
			takeSnap();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public void takeSnap() {
		File src = driver.getScreenshotAs(OutputType.FILE);
		File des = new File("./snaps/img" + i + ".png");
		try {
			FileUtils.copyFile(src, des);
		} catch (IOException e) {
			e.printStackTrace();
		}
		i++;
	}

	@Override
	public void close() {
		driver.close();

	}

	@Override
	public void quit() {
		driver.quit();

	}

	@Override
	public void click(WebElement ele) {
		ele.click();
		System.out.println("The element " + ele + " clicked successfully");
		takeSnap();

	}

	@Override
	public void append(WebElement ele, String data) {

	}

	@Override
	public void clear(WebElement ele) {
		ele.clear();
		System.out.println("The element " + ele + " is cleared successfully");
		takeSnap();

	}

	@Override
	public void clearAndType(WebElement ele, String data) {
		ele.clear();
		ele.sendKeys(data);
		System.out.println("The data " + data + " entered successfully");
		takeSnap();
	}

	@Override
	public String getElementText(WebElement ele) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getBackgroundColor(WebElement ele) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getTypedText(WebElement ele) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void selectDropDownUsingText(WebElement ele, String value) {
		// TODO Auto-generated method stub

	}

	@Override
	public void selectDropDownUsingIndex(WebElement ele, int index) {
		Select sel = new Select(ele);
		sel.selectByIndex(index);

	}

	@Override
	public void selectDropDownUsingValue(WebElement ele, String value) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean verifyExactText(WebElement ele, String expectedText) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean verifyPartialText(WebElement ele, String expectedText) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean verifyExactAttribute(WebElement ele, String attribute, String value) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void verifyPartialAttribute(WebElement ele, String attribute, String value) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean verifyDisplayed(WebElement ele) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean verifyDisappeared(WebElement ele) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean verifyEnabled(WebElement ele) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean verifySelected(WebElement ele) {
		// TODO Auto-generated method stub
		return false;
	}

	static ExtentHtmlReporter html;
	static ExtentReports extent;
	ExtentTest test;

	public void startReport() {
		html = new ExtentHtmlReporter("./report/extentReport.html");
		extent = new ExtentReports();
		html.setAppendExisting(true);
		extent.attachReporter(html);
	}

	public void initializeTest(String testcaseName, String testDec, String author, String category) {
		test = extent.createTest(testcaseName, testDec);
		test.assignAuthor(author);
		test.assignCategory(category);

	}

	public void logStep(String des, String status) {
		if (status.equalsIgnoreCase("pass")) {
			test.pass(des);
		} else if (status.equalsIgnoreCase("fail")) {
			test.fail(des);
			throw new RuntimeException();
		} else if (status.equalsIgnoreCase("warning")) {
			test.warning(des);
		}

	}

	public void endReport() {
		extent.flush();
	}
}